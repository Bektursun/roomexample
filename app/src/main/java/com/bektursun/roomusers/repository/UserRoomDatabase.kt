package com.bektursun.roomusers.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.bektursun.roomusers.repository.dao.UserDao
import com.bektursun.roomusers.repository.model.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [User::class], version = 1)
public abstract class UserRoomDatabase : RoomDatabase() {

    private class UserDatabaseCallback(private val scope: CoroutineScope) : RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.userDao())
                }
            }
        }

        suspend fun populateDatabase(userDao: UserDao) {
            userDao.deleteAll()

            val user = User("Beks", "kurmanbekov46@gmail.com", "21", "Bishkek")
            userDao.insert(user)
        }
    }


    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: UserRoomDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): UserRoomDatabase {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext,
                    UserRoomDatabase::class.java,
                    "user_database").fallbackToDestructiveMigration().addCallback(UserDatabaseCallback(scope)).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}