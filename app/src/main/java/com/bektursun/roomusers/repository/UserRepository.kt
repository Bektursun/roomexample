package com.bektursun.roomusers.repository

import androidx.lifecycle.LiveData
import com.bektursun.roomusers.repository.dao.UserDao
import com.bektursun.roomusers.repository.model.User

class UserRepository(private val userDao: UserDao) {

    val allUsers: LiveData<List<User>> = userDao.getAllUsers()

    suspend fun insert(user: User) {
        userDao.insert(user)
    }
}