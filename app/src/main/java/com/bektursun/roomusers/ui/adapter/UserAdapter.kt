package com.bektursun.roomusers.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.roomusers.R
import com.bektursun.roomusers.repository.model.User

class UserAdapter(context: Context) : RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    private var userList = emptyList<User>()
    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_user_item, parent, false)
        return UserViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val current = userList[position]
        holder.usernameItemView.text = current.username
        holder.userEmailItemView.text = current.userEmail
        holder.userAgeItemView.text = current.age.toString()
        holder.userCityItemView.text = current.userCity
    }

    internal fun setUsers(users: List<User>) {
        this.userList = users
        notifyDataSetChanged()
    }

    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val usernameItemView: TextView = itemView.findViewById(R.id.userNameTextView)
        val userEmailItemView: TextView = itemView.findViewById(R.id.userEmailTextView)
        val userAgeItemView: TextView = itemView.findViewById(R.id.userAgeTextView)
        val userCityItemView: TextView = itemView.findViewById(R.id.userCityTextView)
    }
}