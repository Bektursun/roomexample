package com.bektursun.roomusers.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.roomusers.R
import com.bektursun.roomusers.repository.model.User
import com.bektursun.roomusers.ui.activity.NewUserActivity
import com.bektursun.roomusers.ui.adapter.UserAdapter
import com.bektursun.roomusers.viewmodel.UserViewModel

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = UserAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        userViewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        userViewModel.allUsers.observe(this, Observer { users ->
            users.let {
                adapter.setUsers(it)
            }
        })
        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, NewUserActivity::class.java)
            startActivityForResult(intent, newUserActivityRequestCode)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newUserActivityRequestCode && resultCode == Activity.RESULT_OK) {
            data?.let {
                val user = User(it.getStringExtra(NewUserActivity.EXTRA_USER_NAME), it.getStringExtra(NewUserActivity.EXTRA_USER_EMAIL), it.getStringExtra(NewUserActivity.EXTRA_USER_AGE),it.getStringExtra(NewUserActivity.EXTRA_USER_CITY))
                userViewModel.insert(user)
            }
        } else {
            Toast.makeText(applicationContext, "User data not save, fuck you!", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        const val newUserActivityRequestCode = 1
    }
}
