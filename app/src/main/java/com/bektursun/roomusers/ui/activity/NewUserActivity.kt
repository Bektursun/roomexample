package com.bektursun.roomusers.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.bektursun.roomusers.R

class NewUserActivity : AppCompatActivity() {

    private lateinit var editUsernameView: EditText
    private lateinit var editUserEmailView: EditText
    private lateinit var editUserAgeView: EditText
    private lateinit var editUserCityView: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)

        editUsernameView = findViewById(R.id.userNameEditText)
        editUserEmailView = findViewById(R.id.userEmailEditText)
        editUserAgeView = findViewById(R.id.userAgeEditText)
        editUserCityView = findViewById(R.id.userCityEditText)

        val saveButton = findViewById<Button>(R.id.saveUserDataBtn)

        saveButton.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(arrayOf(editUsernameView.text, editUserEmailView.text, editUserAgeView.text, editUserCityView.text).toString())) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val username = editUsernameView.text.toString()
                replyIntent.putExtra(EXTRA_USER_NAME, username)
                val userEmail = editUserEmailView.text.toString()
                replyIntent.putExtra(EXTRA_USER_EMAIL, userEmail)
                val userAge = editUserAgeView.text.toString()
                replyIntent.putExtra(EXTRA_USER_AGE, userAge)
                val userCity = editUserCityView.text.toString()
                replyIntent.putExtra(EXTRA_USER_CITY, userCity)
                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    companion object {
        const val EXTRA_USER_NAME = "com.bektursun.roomusers.USERNAME"
        const val EXTRA_USER_EMAIL = "com.bektursun.roomusers.USER_EMAIL"
        const val EXTRA_USER_AGE = "com.bektursun.roomusers.USER_AGE"
        const val EXTRA_USER_CITY = "com.bektursun.roomusers.USER_CITY"
    }
}