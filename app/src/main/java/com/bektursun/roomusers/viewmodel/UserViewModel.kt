package com.bektursun.roomusers.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.bektursun.roomusers.repository.UserRepository
import com.bektursun.roomusers.repository.UserRoomDatabase
import com.bektursun.roomusers.repository.model.User
import kotlinx.coroutines.launch

class UserViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: UserRepository
    val allUsers: LiveData<List<User>>

    init {
        val usersDao = UserRoomDatabase.getDatabase(application, viewModelScope).userDao()
        repository = UserRepository(usersDao)
        allUsers = repository.allUsers
    }

    fun insert(user: User) = viewModelScope.launch {
        repository.insert(user)
    }
}